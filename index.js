//câu lệnh tương tự import from... dùng để import thư viện express vào project
const express = require('express');
const path = require('path');
//import mongooseJS
const mongoose = require('mongoose');


//import router
const drinkRouter = require('./app/routes/drinkRouter');
const voucherRouter = require('./app/routes/voucherRouter');
const userRouter = require('./app/routes/userRouter');
const orderRouter = require('./app/routes/orderRouter');
const taskNR2_Router = require('./app/routes/taskNR2');
const taskNR5_Router = require('./app/routes/taskNR5');
//khai báo cổng của project
const port = 8000;
//khởi tạo app express
const app = express();
//cấu hình để app đọc được body request dạng json
app.use(express.json());
//cấu hình để app đọc được tiếng việt (UTF8)
app.use(express.urlencoded({
    extended : true
}))
//cấu hình để đọc file tĩnh
app.use(express.static(__dirname + '/views'));

/*---------------TASK NR 5 -----------------------*/
app.get('/devcamp-pizza365/', (req,res) => res.sendFile(path.join(__dirname, '/views/Pizza365 v1.9.html')));

app.use('/devcamp-pizza365/', taskNR5_Router);


/*-----------------------Order Management---------------*/
app.get('/devcamp-pizza365/order-management', (req,res) => res.sendFile(path.join(__dirname, '/views/orderManagement.html')))

/*-------ROUTER DRINK MIDDLEWARE------------*/
app.use('/', drinkRouter);

/*-------ROUTER VOUCHER MIDDLEWARE------------*/
app.use('/', voucherRouter);

/*-------ROUTER USER MIDDLEWARE------------*/
app.use('/', userRouter);

/*-------ROUTER ORDER MIDDLEWARE------------*/
app.use('/', orderRouter)

/*--------ROUTER API SKIP LIMIT SORT --------------*/
app.use('/', taskNR2_Router)

//connect to MongoDB
const uri = 'mongodb://localhost:27017/CRUD_PIZZA365'
mongoose.connect(uri, (error) => {
    if (error) throw error;
    console.log('MongoDB connected successfully!')
})

//chạy app express trên port
app.listen(port, () => {console.log(`App is listening on port ${port}`)});
