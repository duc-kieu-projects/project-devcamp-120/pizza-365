const taskNR5_Router = require('express').Router();
const drinkModel = require('../models/drinkModel');
const voucherModel = require('../models/voucherModel');

//get all drinks
taskNR5_Router.post('/drinks',  (req,res) => {
    drinkModel.find( (err,drinkList) => {
        if (err) {
            return res.status(500).json({message: `Internal server error: ${err.message}`})
        } else {
            return res.status(200).json(drinkList)
        }
    })
});

//kiểm tra voucher
taskNR5_Router.get('/vouchers', (req,res) => {
    let maVoucher = req.query.maVoucher;
    let condition = {};
    if (maVoucher) {
        condition.maVoucher = maVoucher
    };
    voucherModel.find(condition, (err, voucherFound) => {
        if (err) {
            return res.status(500).json({message: `Internal server error: ${err.message}`})
        } else {
            return res.status(200).json(voucherFound)
        }
    })
})

module.exports = taskNR5_Router;
