//import thư viện express
const userRouter = require('express').Router();

//import Controller 
const UserController = require('../controllers/userController');

//get all users
userRouter.get('/users', UserController.getAllUsers);

//create a user
userRouter.post('/users', UserController.createNewUser);

userRouter.get('/users/:userId', UserController.getUserById);

userRouter.put('/users/:userId', UserController.updateUserById);

userRouter.delete('/users/:userId', UserController.deleteUserById);


//export 
module.exports = userRouter