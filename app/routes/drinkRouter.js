//import thư viện express
const drinkRouter = require('express').Router();

//import Controller
const { getAllDrinks, createDrink, getDrinkById, updateDrinkById, deleteDrinkById } = require('../controllers/drinkController');

//get all drinks
drinkRouter.get('/drinks', getAllDrinks);

//create a new drink
drinkRouter.post('/drinks', createDrink);

//get a drink
drinkRouter.get('/drinks/:drinkId', getDrinkById);

//update a drink 
drinkRouter.put('/drinks/:drinkId', updateDrinkById);

// //delete a drink
drinkRouter.delete('/drinks/:drinkId', deleteDrinkById);


//export 
module.exports = drinkRouter