//import thư viện express
const orderRouter = require('express').Router();

//import Controller
const OrderController = require('../controllers/orderController');



orderRouter.get('/orders', OrderController.getAllOrders);

//create new order
orderRouter.post('/users/:userId/orders', OrderController.createNewOrder);

orderRouter.get('/devcamp-pizza365/:orderId', OrderController.getOrderById);

orderRouter.put('/devcamp-pizza365/orders/:orderId', OrderController.updateOrderById);

orderRouter.delete('/users/:userId/orders/:orderId', OrderController.deleteOrderById);

orderRouter.delete('/devcamp-pizza365/orders', OrderController.deleteOrderByOrderCode);


//create order by random orderCode
orderRouter.post('/devcamp-pizza365/orders', OrderController.createNewOrderByRandomOrderCode)

//export 
module.exports = orderRouter