//import mongooseJS
const mongoose = require('mongoose');


//khai báo schema từ mongooseJS
const Schema = mongoose.Schema;

//khởi tạo order schema
const orderSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
    },
    orderCode: {
        type: String,
        unique: true, 
    },
    pizzaSize: {
        type: String,
        required: true,
    },
    pizzaType: {
        type: String,
        required: true,
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: 'Voucher'
    },
    drink: {
        type: mongoose.Types.ObjectId,
        ref: 'Drink'
    },
    status: {
        type: String,
        required: true,
    },
    ngayTao: {
        type: Date,
        default: Date.now(),
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now(),
    },
});

//export ra module
module.exports = mongoose.model('Order', orderSchema);

