//import mongooseJS
const mongoose = require('mongoose');

//khai báo schema từ mongooseJS
const Schema = mongoose.Schema;

//khởi tạo drink schema
const drinkSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
    },
    maNuocUong: {
        type: String,
        unique: true, 
        required: true,
    },
    tenNuocUong: {
        type: String,
        required: true,
    },
    donGia: {
        type: Number,
        required: true,
    },
    ngayTao: {
        type: Date,
        default: Date.now(),
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now(),
    },
});

//export ra module
module.exports = mongoose.model('Drink', drinkSchema);
