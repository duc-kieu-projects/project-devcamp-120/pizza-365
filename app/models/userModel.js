//import mongooseJS
const mongoose = require('mongoose');

//khai báo schema từ mongooseJS
const Schema = mongoose.Schema;

//khởi tạo user schema
const userSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
    },
    fullName: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        unique: true,
        required: true,
    },
    address: {
        type: String,
        required: true,
    },
    phone: {
        type: String,
        unique: true,
        required: true,
    },
    orders: [
        {
            type:  mongoose.Types.ObjectId,
            ref: 'Order'
        }
    ],
    ngayTao: {
        type: Date,
        default: Date.now(),
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now(),
    },
});

//export ra module
module.exports = mongoose.model('User', userSchema);
