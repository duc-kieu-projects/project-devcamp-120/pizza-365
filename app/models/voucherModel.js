//import mongooseJS
const mongoose = require('mongoose');

//khai báo schema từ mongooseJS
const Schema = mongoose.Schema;

//khởi tạo voucher schema
const voucherSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
    },
    maVoucher: {
        type: String,
        unique: true, 
        required: true,
    },
    phanTramGiamGia: {
        type: Number,
        required: true,
    },
    ghiChu: {
        type: String,
        required: false,
    },
    ngayTao: {
        type: Date,
        default: Date.now(),
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now(),
    },
});

//export ra module
module.exports = mongoose.model('Voucher', voucherSchema);
