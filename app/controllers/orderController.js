const orderModel = require('../models/orderModel');
const userModel = require('../models/userModel');
const voucherModel = require('../models/voucherModel');
const drinkModel = require('../models/drinkModel');
const mongoose = require('mongoose');

const OrderController = {
    //create new order 
    createNewOrder: (req,res) => {
        //B1 : thu thập dữ liệu
        let userId = req.params.userId;
        let bodyReq = req.body;
        let newOrder = {
            _id: mongoose.Types.ObjectId(),
            orderCode: bodyReq.orderCode,
            pizzaSize: bodyReq.pizzaSize,
            pizzaType: bodyReq.pizzaType,
            voucher: bodyReq.voucher,
            drink: bodyReq.drink,
            status: bodyReq.status,
        }
        //B2 : kiểm tra dữ liệu
        if(!mongoose.Types.ObjectId.isValid(userId)) {
            return response.status(400).json({
                status: "Error 400: Bad Request",
                message: "User ID is invalid"
            })
        };

        if (!(mongoose.Types.ObjectId.isValid(newOrder.voucher) || mongoose.Types.ObjectId.isValid(newOrder.drink))) {
            return res.status(400).json({
                status: 'ERROR 400: Bad Request',
                message: 'Voucher or Drink are not valid!'
            })
        };
        //B3 : thao tác với CSDL
        if (mongoose.Types.ObjectId.isValid(newOrder.voucher) || mongoose.Types.ObjectId.isValid(newOrder.drink)) {
                orderModel.create(newOrder, (err,data) => {
                    if(err) {
                        return res.status(500).json({
                            status: 'Error 500: Internal server error',
                            message: err.message
                        })
                    }
                    else{
                        userModel.findByIdAndUpdate(userId,
                        {
                            $push: { orders: data._id }
                        },
                        (err, updatedUser) => {
                            if (err) {
                                return res.status(500).json({
                                    status: "Error 500: Internal server error",
                                    message: err.message
                                })                            
                            }else {
                                return res.status(201).json({
                                    status: 'Created Order successfully!',
                                    data: data
                                })
                            }
                        }
                        )
                                        
                    } 
                })            
        }
    },

    //create new order by random orderCode and check email
    createNewOrderByRandomOrderCode: (req,res) => {
        //B1: cbi dữ liệu
        let fullName = req.body.fullName;
        let email = req.body.email;
        let address = req.body.address;
        let phone = req.body.phone;
        let pizzaSize = req.body.pizzaSize;
        let pizzaType = req.body.pizzaType;
        let voucher = req.body.voucher;
        let drink = req.body.drink;
        let status = req.body.status;
        //random OrderCode
        let randomOrderCode = Math.random().toString(36).substring(2,12);
        //B2: kiểm tra xem email đã tồn tại hay chưa
        userModel.findOne( { email: req.body.email }, (errFind, emailExist) => {
            if (errFind) {
               return res.status(500).json({message: `Internal server error: ${errFind.message}`})
            } else {
                //nếu email chưa tồn tại, tạo 1 user mới theo email đã cho
                if (!emailExist) {
                    userModel.create({
                        _id: mongoose.Types.ObjectId(),
                        fullName,
                        email,
                        address,
                        phone
                    }, (err,newUser) => {
                        if (err) {
                            return res.status(500).json({message: `Internal server error: ${err.message}`});
                        } else {
                            //lấy userId để tạo order
                            orderModel.create({
                                _id: mongoose.Types.ObjectId(),
                                orderCode: randomOrderCode,
                                pizzaSize,
                                pizzaType,
                                voucher,
                                drink,
                                status
                            }, (err,newOrder) => {
                                if (err) {
                                    return res.status(500).json({message: `Internal server error: ${err.message}`});
                                } else {
                                    userModel.findByIdAndUpdate(newUser._id, {
                                        $push: { orders: newOrder._id }
                                    },
                                    (err, updatedUser) => {
                                        if (err) {
                                            return res.status(500).json({
                                                status: "Error 500: Internal server error",
                                                message: err.message
                                            })                            
                                        }else {
                                            return res.status(201).json(newOrder);
                                        }
                                    })
                                }
                            })
                        }
                    })
                } else {
                    //tạo order từ email đã có
                    orderModel.create({
                        _id: mongoose.Types.ObjectId(),
                        orderCode: randomOrderCode,
                        pizzaSize,
                        pizzaType,
                        voucher,
                        drink,
                        status
                    }, (err,newOrder) => {
                        if (err) {
                            return res.status(500).json({message: `Internal server error: ${err.message}`});
                        } else {
                            userModel.findByIdAndUpdate(emailExist._id, {
                                $push: { orders: newOrder._id }
                            },
                            (err, updatedUser) => {
                                if (err) {
                                    return res.status(500).json({
                                        status: "Error 500: Internal server error",
                                        message: err.message
                                    })                            
                                }else {
                                    return res.status(201).json(newOrder);
                                }
                            })
                        }
                    })
                }

            }
        })
        
    },

    //GET all orders
    getAllOrders: (req,res) => {
        orderModel.find((err,data) => {
            res.status(200).json(data)
        }).populate('drink').populate('voucher');
    },

    //GET order by Id
    getOrderById: (req,res) => {
        //B1 : 
        let orderId = req.params.orderId;
        //B2 : 
        if (!mongoose.Types.ObjectId.isValid(orderId)) {
            return res.status(400).json({
                status: 'ERROR 400: Bad Request',
                message: 'Order Id is invalid!'
            })
        }else {
            orderModel.findById(orderId,(err,data) => {
                if (err) {
                    return res.status(500).json({
                        status: 'Error 500: Internal server error',
                        message: err.message
                    })
                } else {
                    return res.status(200).json({
                        status: 'Order Found!',
                        Order: data
                    })
                }
            })
        }
    },

    //Update order by Id
    updateOrderById: (req,res) => {
        //B1 : 
        let orderId = req.params.orderId;
        let bodyReq = req.body;
        let orderUpdated = {
            orderCode: bodyReq.orderCode,
            pizzaSize: bodyReq.pizzaSize,
            pizzaType: bodyReq.pizzaType,
            drink: bodyReq.drink,
            voucher: bodyReq.voucher,
            status: bodyReq.status,
        }
        //B2 : 
        if (!mongoose.Types.ObjectId.isValid(orderId)) {
            return res.status(400).json({
                status: 'ERROR 400: Bad Request',
                message: 'Order Id is invalid!'
            })
        }else {
            orderModel.findByIdAndUpdate(orderId, orderUpdated, (err,data) => {
                if (err) {
                    return res.status(500).json({
                        status: 'Error 500: Internal server error',
                        message: err.message
                    })
                } else {
                    return res.status(200).json(data)
                }
            })
        }    
    },

    //delete order by Id
    deleteOrderById: (req,res) => {
        //B1: thu thập dữ liệu
        let userId = req.params.userId;
        let orderId = req.params.orderId
        //B2: kiểm tra dữ liệu
        if (!mongoose.Types.ObjectId.isValid(userId)) {
            return res.status(400).json({
                status: 'Error 400: Bad request',
                message: 'User Id is not valid!'
            })
        };

        if (!mongoose.Types.ObjectId.isValid(orderId)) {
            return res.status(400).json({
                status: 'Error 400: Bad request',
                message: 'Order Id is not valid!'
            })
        };
        //B3: thao tác với CSDL
        orderModel.findByIdAndDelete(orderId, (err,data) => {
            if (err) {
                return res.status(500).json({
                    status: 'Error 500: Internal server error',
                    message: err.message
                })
            }   
            else {
                userModel.findByIdAndUpdate(userId,
                {
                    $pull: { orders: orderId }
                },
                (err, userUpdated) => {
                    if (err) {
                        return res.status(500).json({
                            status: 'Error 500: Internal server error',
                            message: err.message
                        })
                    }else {
                        return res.status(200).json({
                            status: 'Order Deleted Successfully!',
                            'User updated': userUpdated
                        })
                    }
                })
            }
        })
    },

    //delete order by orderCode
    deleteOrderByOrderCode: (req,res) => {
        let orderCode = req.query.orderCode;
        orderModel.findOne( { orderCode }, (err,orderExist) => {
            if (err) {
                return res.status(500).json({message:`Internal server error:${err.message}`})
            } else {
                orderModel.findByIdAndDelete(orderExist._id, (err,data) => {
                    if (err) {
                        return res.status(500).json({message:`Internal server error:${err.message}`})
                    } else {
                        userModel.findOne( { orders: orderExist._id }, (err,userHasOrder) => {
                            if (err) {
                                return res.status(500).json({message:`Internal server error:${err.message}`})
                            } else {
                                userModel.findByIdAndUpdate(userHasOrder._id,
                                     { $pull: { orders: orderExist._id } }, (err, userUpdated) => {
                                        if (err) {
                                            return res.status(500).json({message:`Internal server error:${err.message}`})
                                        } else {
                                            return res.status(200).json(userUpdated)
                                        }
                                     })
                            }
                        })
                    }
                })
            }
        })
    },

    // //delete order by orderID
    // deleteOrderByOrderId: (req,res) => {
    //     let orderId = req.params.orderId;
    //     orderModel.findByIdAndDelete(orderId, (err,data) => {
    //         if (err) {
    //             return res.status(500).json({message:`Internal server error:${err.message}`})
    //         } else {
    //             orderModel.findOne({ _id: orderId }, )
    //         }
    //     })
    // }
};

module.exports = OrderController;
