//import drinkModel 
const drinkModel = require('../models/drinkModel');

//import thư viện mongooseJS
const mongoose = require('mongoose');

//get all drink
const getAllDrinks = (req,res) => {
    let maNuocUong = req.query.maNuocUong
    if (maNuocUong) {
        drinkModel.find( {maNuocUong }, (err,data) => {
            if (err) {
                return res.status(500).json({message:`Internal server error: ${err.message}`})
            } else {
                return res.status(200).json(data)
            }
        })
    } else {
        drinkModel.find( (err,data) => {
            if (err) {
                return res.status(500).json({message:`Internal server error: ${err.message}`})
            } else {
                return res.status(200).json(data)
            }
        })
    }
}

//create drink
const createDrink = (req,res) => {
    //B1: thu thập dữ liệu
    let bodyReq = req.body;
    //B2: kiểm tra dữ liệu
    if (!(bodyReq.maNuocUong && bodyReq.tenNuocUong)) {
        return res.status(400).json({
            status: 'Error 400: Bad request',
            message: 'maNuocUong & tenNuocUong is required'
        })
    };
    if (!(Number.isInteger(bodyReq.donGia) && bodyReq.donGia > 0)) {
        return res.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Don Gia is not valid'
        })
    };
    //B3: thao tác với CSDL
    let newDrink = {
        _id: mongoose.Types.ObjectId(),
        maNuocUong: bodyReq.maNuocUong,
        tenNuocUong: bodyReq.tenNuocUong,
        donGia: bodyReq.donGia
    }
    
    drinkModel.create(newDrink, (err,data) => {
        if(err) {
            return res.status(500).json({
                status: 'Error 500: Internal server error',
                message: err.message
            })
        }else
        {
            return res.status(201).json({
                status: 'Created Drink successfully!',
                data: data
            })
        } 
    })
}

//get a drink 
const getDrinkById = (req,res) => {
    //B1: thu thập dữ liệu
    let drinkId = req.params.drinkId;
    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        res.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Drink Id is not valid!'
        })
    }
    else {
        drinkModel.findById(drinkId, (err,data) => {
            if (err) {
                res.status(500).json({
                    status: 'Error 500: Internal server timeout',
                    message: err.message
                })
            }
            return res.status(200).json({
                status: 'Drink Found!',
                drinkFound: data
            })
        })
    }
}

//update a drink
const updateDrinkById = (req,res) => {
    //B1: thu thập dữ liệu
    let drinkId = req.params.drinkId;
    let bodyReq = req.body;
    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        res.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Drink Id is not valid!'
        })
    };
    //B3: thao tác với CSDL
    let drinkUpdated = {
        maNuocUong: bodyReq.maNuocUong,
        tenNuocUong: bodyReq.tenNuocUong,
        donGia: bodyReq.donGia
    }
    drinkModel.findByIdAndUpdate(drinkId, drinkUpdated, (err,data) => {
        if (err) {
            res.status(500).json({
                status: 'Error 500: Internal server timeout',
                message: err.message
            })
        }
        else {
            res.status(200).json({
                status: 'Drink Updated Successfully!',
                drinkUpdated
            })
        }
    })
}

//delete a drink 
const deleteDrinkById = (req,res) => {
    //B1: thu thập dữ liệu
    let drinkId = req.params.drinkId;
    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        res.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Drink Id is not valid!'
        })
    }
    else {
        drinkModel.findByIdAndDelete(drinkId, (err,data) => {
            if (err) {
                res.status(500).json({
                    status: 'Error 500: Internal server timeout',
                    message: err.message
                })
            }
            return res.status(204).json({
                message: 'Drink Deleted!',
            })
        })
    }
}

//export ra module
module.exports = {
    getAllDrinks,
    createDrink,
    getDrinkById,
    updateDrinkById,
    deleteDrinkById
};
