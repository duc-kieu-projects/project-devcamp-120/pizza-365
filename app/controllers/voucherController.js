//import voucherModel 
const voucherModel = require('../models/voucherModel');

//import thư viện mongooseJS
const mongoose = require('mongoose');

//get all voucher
const getAllVouchers = (req,res) => {
    voucherModel.find((err,data) => {
        res.status(200).json({
            status: 'Get All vouchers',
            voucherList: data
        })
    })
}

//create voucher
const createVoucher = (req,res) => {
    //B1: thu thập dữ liệu
    let bodyReq = req.body;
    //B2: kiểm tra dữ liệu
    if (!(bodyReq.maVoucher && bodyReq.phanTramGiamGia)) {
        res.status(400).json({
            status: 'Error 400: Bad request',
            message: 'maVoucher & phanTramGiamGia is required'
        })
    };
    if (!(Number.isInteger(bodyReq.phanTramGiamGia))) {
        res.status(400).json({
            status: 'Error 400: Bad request',
            message: 'phanTramGiamGia is not valid'
        })
    };
    //B3: thao tác với CSDL
    let newVoucher = {
        _id: mongoose.Types.ObjectId(),
        maVoucher: bodyReq.maVoucher,
        phanTramGiamGia: bodyReq.phanTramGiamGia,
    }
    
    voucherModel.create(newVoucher, (err,data) => {
        if(err) {
            res.status(500).json({
                status: 'Error 500: Internal server error',
                message: err.message
            })
        }else
        {
            res.status(201).json({
                status: 'Created voucher successfully!',
                data: data
            })
        } 
    })
}

//get a voucher 
const getVoucherById = (req,res) => {
    //B1: thu thập dữ liệu
    let voucherId = req.params.voucherId;
    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        res.status(400).json({
            status: 'Error 400: Bad request',
            message: 'voucher Id is not valid!'
        })
    }
    else {
        voucherModel.findById(voucherId, (err,data) => {
            if (err) {
                res.status(500).json({
                    status: 'Error 500: Internal server timeout',
                    message: err.message
                })
            }
            return res.status(200).json({
                status: 'voucher Found!',
                voucherFound: data
            })
        })
    }
}

//update a voucher
const updateVoucherById = (req,res) => {
    //B1: thu thập dữ liệu
    let voucherId = req.params.voucherId;
    let bodyReq = req.body;
    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        res.status(400).json({
            status: 'Error 400: Bad request',
            message: 'voucher Id is not valid!'
        })
    };
    //B3: thao tác với CSDL
    let voucherUpdated = {
        maVoucher: bodyReq.maVoucher,
        phanTramGiamGia: bodyReq.phanTramGiamGia,
        ghiChu: bodyReq.ghiChu
    }
    voucherModel.findByIdAndUpdate(voucherId, voucherUpdated, (err,data) => {
        if (err) {
            res.status(500).json({
                status: 'Error 500: Internal server timeout',
                message: err.message
            })
        }
        else {
            res.status(200).json({
                status: 'voucher Updated Successfully!',
                voucherUpdated
            })
        }
    })
}

//delete a voucher 
const deleteVoucherById = (req,res) => {
    //B1: thu thập dữ liệu
    let voucherId = req.params.voucherId;
    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        res.status(400).json({
            status: 'Error 400: Bad request',
            message: 'voucher Id is not valid!'
        })
    }
    else {
        voucherModel.findByIdAndDelete(voucherId, (err,data) => {
            if (err) {
                res.status(500).json({
                    status: 'Error 500: Internal server timeout',
                    message: err.message
                })
            }
            return res.status(204).json({
                message: 'voucher Deleted!',
            })
        })
    }
}

//export ra module
module.exports = {
    getAllVouchers,
    createVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
};
