$(document).ready(function() {
    "use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
   
    // constructor object for Menu Structure Objects:
    function gMenuSelected(menuName,duongKinhCM,suonNuong,saladGr,drink,priceVND) {
      this.menuName = menuName,    // S, M, L
      this.duongKinhCM = duongKinhCM,
      this.suonNuong = suonNuong,
      this.saladGr = saladGr,
      this.drink = drink,
      this.priceVND = priceVND
    }

    //biến dùng để lưu menu combo được chọn, mỗi khi khách chọn, giá trị được thay đổi
    var gMenuSelectedObj = {
        menuName: "",
        duongKinhCM: "",
        suonNuong: "",
        saladGr: "",
        drink: "",
        priceVND: ""
    };
    // biến dùng để lưu loại pizza đươc chọn, mỗi khi khách chọn, giá trị được thay đổi
    var gSelectedPizzaType = "";

    // biến dùng để lưu size pizza đươc chọn, mỗi khi khách chọn, giá trị được thay đổi
    var gSelectedPizzaSize = "";

    // tạo 1 obj lưu dữ liệu order 
    var gClient = {
        fullName : "",
        email: "",
        phone: "",
        address: "",
        message: "",
        voucher: "",
        drink: "",
        paid: "",
        pizzaSize: "",
        pizzaType: ""
    }

    //biến lưu giá trị Id của drink
    var gDrinkID = '';

    //biến lưu giá trị Id của voucher
    var gVoucherID = null;

    //lưu giá trị thanh toán
    var gPaid = 0;

    //tạo 1 obj lưu giá trị thanh toán với voucher
    var gPrice = {
        voucherId : "",
        percentDiscount : 0
    }

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    
    //sự kiện load dữ liệu đồ uống 
    loadDataDrinks()

    //sự kiện chọn combo size S
    $("#btn-small").on("click", function() {
        onBtnSmallClick();
    })
    
    //sự kiện chọn combo size M
     $("#btn-medium").on("click", function() {
        onBtnMediumClick();
    })

    //sự kiện chọn combo size L
     $("#btn-large").on("click", function() {
        onBtnLargeClick();
    })

    //sự kiện chọn pizza seafood
    $("#pizza-seafood").on("click", function() {
        onBtnPizzaSeafood()
    })

    //sự kiện chọn pizza hawaiian
    $("#pizza-hawaii").on("click", function() {
        onBtnPizzaHawaiian()
    })

    //sự kiện chọn pizza bacon
     $("#pizza-bacon").on("click", function() {
        onBtnPizzaBacon()
    })

    //sự kiện gửi đơn hàng
    $("#btn-create-order").on("click", function() {
        onBtnCreateOrderClick();
    });

    // sự kiện on change lấy dữ liệu drink
    $('#input-drink').on('change', function() {
        let drink = $('#input-drink').val()
        $.ajax({
            url: '/drinks?maNuocUong=' + drink,
            type: 'GET',
            success: function(res) {
                gDrinkID = res[0]._id;
            },
            error: function(err) {
                alert(err.responseText)
            }
        })
    })

    //sự kiện on change kiểm tra voucher
    $('#inp-voucher').on('change', function() {
        var paramNumber = $('#inp-voucher').val()
        checkVoucherID(paramNumber);
    });

    //sự kiện click nút tạo đơn hàng trên modal
    $("#btn-create-order-modal").on("click", function() {
        onBtnCreateOrderModalClick();
    })

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    
    // hàm Chọn Combo Small
    function onBtnSmallClick() {
      "use strict";
        console.log("%c Chọn Combo là: ", "color:red");
        gMenuSelectedObj = new gMenuSelected("Small","20cm","2","200gr","2","150000");
    
        gSelectedPizzaSize = gMenuSelectedObj.menuName;
        //đổi màu nút được chọn
        changeBtnColor(gSelectedPizzaSize);

        gPaid = gMenuSelectedObj.priceVND;
        console.log("PizzaSize được chọn: " + gSelectedPizzaSize);
        console.log("Price: " + gPaid);

    }

    // hàm Chọn Combo Medium
    function onBtnMediumClick() {
        "use strict";
          console.log("%c Chọn Combo là: ", "color:red");
          gMenuSelectedObj = new gMenuSelected("Medium","25cm","4","300gr","3","200000");
      
          gSelectedPizzaSize = gMenuSelectedObj.menuName;
          //đổi màu nút được chọn
          changeBtnColor(gSelectedPizzaSize);
  
          gPaid = gMenuSelectedObj.priceVND;
          console.log("PizzaSize được chọn: " + gSelectedPizzaSize);
          console.log("Price: " + gPaid);
  
      }

    // hàm Chọn Combo Medium
    function onBtnLargeClick() {
        "use strict";
          console.log("%c Chọn Combo là: ", "color:red");
          gMenuSelectedObj = new gMenuSelected("Large","30cm","8","500gr","4","250000");
      
          gSelectedPizzaSize = gMenuSelectedObj.menuName;
        //đổi màu nút được chọn
        changeBtnColor(gSelectedPizzaSize);

        gPaid = gMenuSelectedObj.priceVND;
        console.log("PizzaSize được chọn: " + gSelectedPizzaSize);
        console.log("Price: " + gPaid);
  
      }
    
    // hàm chọn Loại Pizza seafood
    function onBtnPizzaSeafood() {
        "use strict";
        console.log("%c Chọn Loại Pizza Clicked!", "color:blue")
        // gán giá trị của pizza được chọn vào biến toàn cục để lưu tại đó
        gSelectedPizzaType = "Seafood";
        console.log("Pizza đã chọn: " + gSelectedPizzaType);
        // đổi màu button
        changePizzaButtonColor(gSelectedPizzaType);
      }
    
    // hàm chọn Loại Pizza hawaiian
    function onBtnPizzaHawaiian() {
      "use strict";
      console.log("%c Chọn Loại Pizza Clicked!", "color:blue")
      // gán giá trị của pizza được chọn vào biến toàn cục để lưu tại đó
      gSelectedPizzaType = "Hawaii";
      console.log("Pizza đã chọn: " + gSelectedPizzaType);
      // đổi màu button
      changePizzaButtonColor(gSelectedPizzaType);
    }
    
    // hàm chọn Loại Pizza Bacon
    function onBtnPizzaBacon() {
      "use strict";
      console.log("%c Chọn Loại Pizza Clicked!", "color:blue")
      // gán giá trị của pizza được chọn vào biến toàn cục để lưu tại đó
      gSelectedPizzaType = "Bacon";
      console.log("Pizza đã chọn: " + gSelectedPizzaType);
      // đổi màu button
      changePizzaButtonColor(gSelectedPizzaType);
    }

    //hàm tạo order 
    function onBtnCreateOrderClick() {
        "use strict";
        console.log("%c gửi đơn hàng Clicked!", "color:green");
        //B1 : thu thập dữ liệu
        getData(gClient);
        //B2 : kiểm tra dữ liệu
        var vCheck = validateData(gClient);
        if ( vCheck == true ) {
            //B3 : Hiển thị Dữ liệu lên modal
            $("#create-order-modal").modal("show");
            displayOrderInModal(gClient);
        }
    }

    //hàm gửi đơn hàng trên modal
    function onBtnCreateOrderModalClick() {
        "use strict";
        console.log("%c Gửi Đơn Hàng!", "color:blue");
        $("#create-order-modal").modal("hide");
        // khai báo object order để chứa thông tin đặt hàng
        if (gVoucherID == null) {
            var vNewOrder = {
                pizzaSize:  gSelectedPizzaSize,
                pizzaType: gSelectedPizzaType,
                drink: gDrinkID,
                fullName: gClient.fullName,
                email: gClient.email,
                phone: gClient.phone,
                address: gClient.address,
                status: 'open'
            }
            getAjaxAPICreateOrder(vNewOrder);
        } else {
            var vNewOrder = {
                pizzaSize:  gSelectedPizzaSize,
                pizzaType: gSelectedPizzaType,
                voucher: gVoucherID,
                drink: gDrinkID,
                fullName: gClient.fullName,
                email: gClient.email,
                phone: gClient.phone,
                address: gClient.address,
                status: 'open'
            }
            getAjaxAPICreateOrder(vNewOrder);
        }
        
    }
    
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // hàm đổi màu nút chọn Combo
    function changeBtnColor(paramSelectedComboName) {
      "use strict";
        if (paramSelectedComboName === "Small") {
            $("#btn-small").removeClass("btn-warning");
            $("#btn-small").addClass("btn-success");
            $("#btn-medium").addClass("btn-warning");
            $("#btn-medium").removeClass("btn-success");
            $("#btn-large").addClass("btn-warning");
            $("#btn-large").removeClass("btn-success");
        };
        if (paramSelectedComboName === "Medium") {
            $("#btn-small").addClass("btn-warning");
            $("#btn-small").removeClass("btn-success");
            $("#btn-medium").removeClass("btn-warning");
            $("#btn-medium").addClass("btn-success");
            $("#btn-large").addClass("btn-warning");
            $("#btn-large").removeClass("btn-success");
        };
        if (paramSelectedComboName === "Large") {
            $("#btn-small").addClass("btn-warning");
            $("#btn-small").removeClass("btn-success");
            $("#btn-medium").addClass("btn-warning");
            $("#btn-medium").removeClass("btn-success");
            $("#btn-large").removeClass("btn-warning");
            $("#btn-large").addClass("btn-success");
        }
      
    }
  
    //hàm đổi màu nút chọn Pizza
    function changePizzaButtonColor(paramSelectedPizzaType) {
      "use strict";
        if (paramSelectedPizzaType === "Seafood") {
            $("#pizza-seafood").removeClass("btn-warning");
            $("#pizza-seafood").addClass("btn-success");
            $("#pizza-hawaii").addClass("btn-warning");
            $("#pizza-hawaii").removeClass("btn-success");
            $("#pizza-bacon").addClass("btn-warning");
            $("#pizza-bacon").removeClass("btn-success");
        };
        if (paramSelectedPizzaType === "Hawaii") {
            $("#pizza-seafood").addClass("btn-warning");
            $("#pizza-seafood").removeClass("btn-success");
            $("#pizza-hawaii").removeClass("btn-warning");
            $("#pizza-hawaii").addClass("btn-success");
            $("#pizza-bacon").addClass("btn-warning");
            $("#pizza-bacon").removeClass("btn-success");
        };
        if (paramSelectedPizzaType === "Bacon") {
            $("#pizza-seafood").addClass("btn-warning");
            $("#pizza-seafood").removeClass("btn-success");
            $("#pizza-hawaii").addClass("btn-warning");
            $("#pizza-hawaii").removeClass("btn-success");
            $("#pizza-bacon").removeClass("btn-warning");
            $("#pizza-bacon").addClass("btn-success");
        }
    }

    //hàm gọi api lấy dữ liệu drink
    function loadDataDrinks() {
        console.log("load drink data!");
        $.ajax({
            url: "/devcamp-pizza365/drinks",
            dataType: 'json',
            type: 'POST',
            success: function(responseObj) {
                handleDrinkList(responseObj);
            },
            error: function(errorContent) {
                alert(errorContent.responseText);
            }
        });
    }

    //hàm tạo option cho đồ uống
    function handleDrinkList(paramResponseObj) {
        "use strict";
        $.each(paramResponseObj, function(i,item) {
            $("#input-drink").append($('<option/>',{
                text: item.tenNuocUong,
                value: item.maNuocUong
            }))
        })
    }
  
    //hàm thu thập dữ liệu 
    function getData(gClient) {
        "use strict";
        gClient.fullName = $("#inp-fullname").val().trim();
        gClient.email = $("#inp-email").val().trim();
        gClient.phone = $("#inp-dien-thoai").val().trim();
        gClient.address = $("#inp-dia-chi").val().trim();
        gClient.message = $("#inp-message").val().trim();
        gClient.voucher = $('#inp-voucher').val().trim();
        gClient.drink = $('#input-drink').val();
        gClient.pizzaSize = gSelectedPizzaSize;
        gClient.pizzaType = gSelectedPizzaType;
        gClient.paid = gPaid;
    }

    //hàm kiểm tra dữ liệu gửi đơn hàng  
    function validateData(gClient) {
        "use strict";
        if ( gClient.fullName == "" ) {
            alert("Vui lòng điền Họ và Tên!");
            return false;
        }
        
        if ( gClient.email == "" ) {
            alert("Vui lòng điền email!");
            return false;
        }

        if ( !validateEmail(gClient.email)) {
            alert("Vui lòng đúng định dạng email!");
            return false;
        }

        if ( gClient.phone == "" ) {
            alert("vui lòng nhập số điện thoại!");
            return false;
        }

        if ( gClient.phone.length > 15 || gClient.phone.length < 9 ) {
            alert("Vui lòng điền số điện thoại hợp lệ!");
            return false;
        }

        if ( gClient.address == "" ) {
            alert("Vui lòng điền địa chỉ nhận hàng!");
            return false;
        }

        if ( gClient.drink == 'all' ) {
            alert("Vui lòng chọn đồ uống!");
            return false;
        };

        if ( gClient.comboSelected == "") {
            alert("vui lòng chọn combo!");
            return false;
        };

        if ( gClient.pizzaTypeSelected == "") {
            alert("vui lòng chọn loại pizza!");
            return false;
        };  

        return true;
    }

    //hàm kiểm tra định dạng email
    function validateEmail(paramEmail) {
        "use strict";
        const vREG = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return vREG.test(String(paramEmail).toLowerCase());
    }

    //hàm kiểm tra mã voucher
    var checkVoucherID = function(paramNumber) {
        console.log(paramNumber);
        $.ajax({
            url: "/devcamp-pizza365/vouchers?maVoucher=" + paramNumber,
            type: 'GET',
            async: true,
            success: function(responsObj) {
                 if (responsObj.length > 0) {
                    gVoucherID = responsObj[0]._id;
                    gPrice.percentDiscount = responsObj[0].phanTramGiamGia;
                    gPrice.voucherId = responsObj[0].maVoucher;
                    $('#inp-voucher').removeClass('is-invalid');
                    $('#inp-voucher').addClass('is-valid');
                    $('#checkVoucher').removeClass('invalid-feedback');
                    $('#checkVoucher').removeClass('text-danger');
                    $('#checkVoucher').addClass('text-success').html(`Bạn có voucher: ${responsObj[0].ghiChu}`)

                } else {
                    gVoucherID == null;
                    gPrice.percentDiscount = 0;
                    $('#inp-voucher').removeClass('is-valid');
                    $('#inp-voucher').addClass('is-invalid');
                    $('#checkVoucher').removeClass('invalid-feedback');
                    $('#checkVoucher').addClass('text-danger').html(`Mã giảm giá không tồn tại`)
                }
                console.log("Phần trăm giảm giá: " + gPrice.percentDiscount);
                console.log("Mã giảm giá: " + gPrice.voucherId);
                },
            error: function(errorContent) {
                  alert(errorContent.responseText);
                  gPrice.percentDiscount = 0;
              }
            })
        }
    
    //hàm tính giá tiền cuối cùng
    function payment(gPaid) {
        if (gPrice.percentDiscount == 0) {
            return gPaid
        } else {
            return gPaid * (100 - gPrice.percentDiscount ) / 100
        }
    }

    //hàm hiển thị thông tin trên modal
    function displayOrderInModal(gClient) {
        "use strict";
        $("#input-fullname-modal").val(gClient.fullName);
        $("#input-phone-modal").val(gClient.phone);
        $("#input-address-modal").val(gClient.address);
        $("#input-message-modal").val(gClient.message);
        $("#input-voucher-modal").val(gClient.voucher);
        $("#input-price-modal").val(payment(gPaid) +" VND");

        $("#input-details-modal").html("Loại pizza: " + gSelectedPizzaType + "." + "\n" 
                                        + "Menu combo đã chọn: " + gSelectedPizzaSize + " gồm :" + "\n"
                                        + gMenuSelectedObj.suonNuong + " sườn nướng, " 
                                        + gMenuSelectedObj.saladGr + " salad." + "\n"
                                        + "Loại đồ uống: " + gClient.drink + ", "
                                        + "số lượng đồ uống: " + gMenuSelectedObj.drink + "." + "\n"
                                        + "Giảm giá: " + gPrice.percentDiscount + " %");
    }

    //hàm gọi api gửi order 
    function getAjaxAPICreateOrder(paramObjectRequest) {
        "use strict";
        $.ajax({
            url: "/devcamp-pizza365/orders",
            type: 'POST',
            async: false,
            data: JSON.stringify(paramObjectRequest),
            dataType: 'json',
            contentType: "application/json;charset=UTF-8",
            success: function(responseObj) {
                displayConfirmOrder(responseObj)
            },
            error: function(errorContent) {
                alert(errorContent.responseText);
            }
        })
    }

    //hàm hiển thị confirm order
    function displayConfirmOrder(paramResponseObj) {
        "use strict";
        console.log(paramResponseObj);
        console.log('Order Code:' + paramResponseObj.orderCode);
        $("#confirm-order-modal").modal("show");
        $("#order-id-modal").val(paramResponseObj.orderCode);
    }
})